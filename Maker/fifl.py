import os

DESCRIPTORS = os.path.join("descriptors")
LEVELS = os.path.join(DESCRIPTORS, "levels")
IMG = os.path.join("img")
AUDIO = os.path.join("audio")
CHARASETS = os.path.join(DESCRIPTORS, "charaset")
FONT = os.path.join("font")

GAMESETTINGS = "init.json"
CHARAS = "charas.json"
