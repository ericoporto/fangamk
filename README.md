![Icon](iconTiny.png) fangamk
=============================

This is a way describe a type of game in json files, a javascipt engine to play it and a pyqt application to make it.

Use WASD to move and IJ to interact in this demo: https://ericoporto.github.io/builds/build00001/index.html

There is a Wiki (I think it's on the right side, I'm on Nano) so click in there because I will be updating there more often.

Right now this code is using PyQt on the maker side, so assume everything here is GPLv2 until I decide better.
